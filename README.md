# Examen-1

Examen CI/CD - Par [Deruel Thomas](https://gitlab.com/tderuel)

## Tâches à effectuer

- [x] Ajout commit inital

- [x] Valider les tests unitaires

- [x] Packager le code dans un livrable

- [x] Déployer le code, uniquement sur MASTER

Projet Terminé.